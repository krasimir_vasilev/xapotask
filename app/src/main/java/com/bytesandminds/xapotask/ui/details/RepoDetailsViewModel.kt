package com.bytesandminds.xapotask.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bytesandminds.xapotask.data.model.RepoDetail
import com.bytesandminds.xapotask.data.network.Result
import com.bytesandminds.xapotask.data.repositories.ReposRepository
import kotlinx.coroutines.Dispatchers

class RepoDetailsViewModel(private val reposRepository: ReposRepository) : ViewModel() {

    fun getRepoDetails(owner: String, repo: String): LiveData<Result<RepoDetail>> {
        return liveData(Dispatchers.IO) {
            val response = reposRepository.getRepoByName(owner, repo)
            emit(response)
        }
    }

}
