package com.bytesandminds.xapotask.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bytesandminds.xapotask.data.model.Repo
import com.bytesandminds.xapotask.data.network.Result
import com.bytesandminds.xapotask.data.repositories.ReposRepository
import kotlinx.coroutines.Dispatchers

class RepositoryListViewModel(private val reposRepository: ReposRepository) : ViewModel() {

    val allRepos: LiveData<Result<List<Repo>>> = liveData(Dispatchers.IO) {
        val response = reposRepository.getAllRepos()
        emit(response)
    }

}
