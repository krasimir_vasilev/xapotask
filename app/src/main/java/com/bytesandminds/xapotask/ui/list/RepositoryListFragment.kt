package com.bytesandminds.xapotask.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bytesandminds.xapotask.R
import com.bytesandminds.xapotask.data.model.Repo
import com.bytesandminds.xapotask.data.network.Result
import kotlinx.android.synthetic.main.repository_list_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class RepositoryListFragment : Fragment(), ReposAdapter.RepoSelectionListener {

    // Lazy Inject ViewModel
    val viewModel: RepositoryListViewModel by sharedViewModel()

    private val adapter = ReposAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.repository_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Set up the RecyclerView
        val linearLayoutManager = LinearLayoutManager(context)
        val divider = DividerItemDecoration(repositoriesRecyclerView.context, linearLayoutManager.orientation)
        repositoriesRecyclerView.layoutManager = linearLayoutManager
        repositoriesRecyclerView.addItemDecoration(divider)
        repositoriesRecyclerView.adapter = adapter

        val reposObserver = Observer<Result<List<Repo>>> {
            updateUI(it)
        }

        //Load repositories from Github server
        viewModel.allRepos.observe(this, reposObserver)
    }

    /**
     * Update the UI depending on the network request result
     *
     * @result from network request to get repositories
     */
    private fun updateUI(result: Result<List<Repo>>) = when (result) {
        is Result.Success -> {
            progressBar.visibility = View.GONE
            noReposTextView.visibility = View.GONE
            adapter.loadData(result.data)
        }
        is Result.Error -> {
            progressBar.visibility = View.GONE
            noReposTextView.visibility = View.VISIBLE
        }
    }

    /**
     * Callback which handle OnItemClickListener event from the RecyclerView
     *
     * @repo selected item from the list
     */
    override fun onRepoSelected(repo: Repo) {
        //Navigate to Repository Details Destination
        val action = RepositoryListFragmentDirections.actionRepositoryListFragmentToRepoDetailsFragment(
            repo.owner.name,
            repo.name
        )
        findNavController().navigate(action)
    }

}
