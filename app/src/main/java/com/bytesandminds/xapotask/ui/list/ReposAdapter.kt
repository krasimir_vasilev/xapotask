package com.bytesandminds.xapotask.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bytesandminds.xapotask.R
import com.bytesandminds.xapotask.data.model.Repo

class ReposAdapter(private val listener: RepoSelectionListener) :
    RecyclerView.Adapter<ReposAdapter.ViewHolder>() {

    private val allRepos = mutableListOf<Repo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = allRepos.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repo = allRepos[position]
        holder.nameTextView.text = repo.name
        holder.fullNameTextView.text = repo.fullName
        holder.isPrivateTextView.text = repo.isPrivate.toString().toUpperCase()
        holder.itemView.setOnClickListener { listener.onRepoSelected(repo) }
    }

    fun loadData(data: List<Repo>) {
        allRepos.clear()
        allRepos.addAll(data)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        val fullNameTextView: TextView = itemView.findViewById(R.id.fullNameTextView)
        val isPrivateTextView: TextView = itemView.findViewById(R.id.isPrivateValueTextView)
    }

    interface RepoSelectionListener {
        fun onRepoSelected(repo: Repo)
    }

}