package com.bytesandminds.xapotask.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bytesandminds.xapotask.R
import com.bytesandminds.xapotask.data.model.RepoDetail
import com.bytesandminds.xapotask.data.network.Result
import kotlinx.android.synthetic.main.repo_details_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class RepoDetailsFragment : Fragment() {

    val args: RepoDetailsFragmentArgs by navArgs()

    val viewModel: RepoDetailsViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.repo_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Load data for a specific Repository
        val owner = args.ownerName
        val repo = args.repoName
        viewModel.getRepoDetails(owner, repo).observe(viewLifecycleOwner, Observer {
            updateUI(it)
        })
    }

    /**
     * Update the UI depending on the network request result
     *
     * @result from network request to get repositories
     */
    private fun updateUI(result: Result<RepoDetail>) = when (result) {
        is Result.Success -> {
            detailsProgressBar.visibility = View.GONE
            noDataTextView.visibility = View.GONE
            populateData(result)
        }
        is Result.Error -> {
            detailsProgressBar.visibility = View.GONE
            noDataTextView.visibility = View.VISIBLE
        }
    }

    private fun populateData(result: Result.Success<RepoDetail>) {
        val repo = result.data
        val owner = repo.owner
        ownerNameTextView.text = owner.name
        repoNameTextView.text = repo.name
        urlTextView.text = repo.url
        descriptionTextView.text = repo.description
        Glide.with(context!!)
            .load(owner.avatarUrl)
            .apply(RequestOptions.circleCropTransform())
            .placeholder(R.mipmap.ic_launcher_round)
            .into(avatarImageView)
    }

}
