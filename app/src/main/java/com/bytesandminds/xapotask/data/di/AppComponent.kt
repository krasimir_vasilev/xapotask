package com.bytesandminds.xapotask.data.di

import org.koin.core.module.Module

val appComponent: List<Module> = listOf(webServiceModule, serializationModule, viewModelModule)