package com.bytesandminds.xapotask.data.network

import com.bytesandminds.xapotask.data.model.Repo
import com.bytesandminds.xapotask.data.model.RepoDetail
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface WebServiceApi {

    @GET("/repositories")
    suspend fun getAllRepos(): Response<List<Repo>>

    @GET("/repos/{owner}/{repo}")
    suspend fun getRepoByName(@Path("owner") owner: String, @Path("repo") repo: String): Response<RepoDetail>
}