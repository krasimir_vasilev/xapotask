package com.bytesandminds.xapotask.data.di

import com.bytesandminds.xapotask.data.repositories.ReposRepository
import com.bytesandminds.xapotask.ui.details.RepoDetailsViewModel
import com.bytesandminds.xapotask.ui.list.RepositoryListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    //Create an instance of ReposRepository
    single { ReposRepository(get()) }
    //Create an instance of RepositoryListVieModel
    viewModel { RepositoryListViewModel(get()) }
    //Create an instance of RepoDetailsViewModel
    viewModel { RepoDetailsViewModel(get()) }
}