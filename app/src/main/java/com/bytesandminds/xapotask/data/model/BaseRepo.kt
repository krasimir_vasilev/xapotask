package com.bytesandminds.xapotask.data.model

interface BaseRepo {
    val name: String
    val fullName: String
    val isPrivate: Boolean
    val owner: Owner
}