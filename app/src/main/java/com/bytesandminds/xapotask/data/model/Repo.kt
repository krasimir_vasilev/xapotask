package com.bytesandminds.xapotask.data.model

import com.squareup.moshi.Json

open class Repo(
    override val name: String,
    @Json(name = "full_name")
    override val fullName: String,
    @Json(name = "private")
    override val isPrivate: Boolean,
    override val owner: Owner
) : BaseRepo