package com.bytesandminds.xapotask.data.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.dsl.module
import java.util.*

val serializationModule = module {
    //Create Moshi Date Adapter
    single { createMoshi() }
}

/**
 * Create a Moshi.BUilder and set Date Adapter which has to care for that
 * how dates from the responses and requests will be parsed.
 */
private fun createMoshi(): Moshi = Moshi.Builder()
    .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
    .add(KotlinJsonAdapterFactory())
    .build()