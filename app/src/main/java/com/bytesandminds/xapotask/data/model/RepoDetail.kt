package com.bytesandminds.xapotask.data.model

import com.squareup.moshi.Json

data class RepoDetail(
    override val name: String,
    @Json(name = "full_name")
    override val fullName: String,
    @Json(name = "private")
    override val isPrivate: Boolean,
    override val owner: Owner,
    val url: String,
    val description: String
) : BaseRepo