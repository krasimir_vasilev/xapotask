package com.bytesandminds.xapotask.data.model

import com.squareup.moshi.Json

data class Owner(
    @Json(name = "login")
    val name: String,
    @Json(name = "avatar_url")
    val avatarUrl: String
)