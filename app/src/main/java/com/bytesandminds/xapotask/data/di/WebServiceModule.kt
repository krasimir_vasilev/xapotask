package com.bytesandminds.xapotask.data.di

import com.bytesandminds.xapotask.BuildConfig
import com.bytesandminds.xapotask.data.network.WebServiceApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import javax.security.cert.CertificateException

private const val defaultTimeout: Long = 60

val webServiceModule = module {
    //Create TrustManagers and SSLSocketFactory to ignore the SSL check
    single { createTrustManagers() }
    single { createSSLSocketFactory(trustAllCerts = get()) }
    //Create HttpLoggingInterceptor
    single { createLoggingInterceptor() }
    //Create OkHttpClient
    single { createOkHttpClient(sslSocketFactory = get(), trustAllCerts = get(), loggingInterceptor = get()) }
    //Create WebServiceApi
    single { createWebServiceApi(okHttpClient = get(), moshi = get()) }
}

/**
 * Setup a Retrofit.Builder and create a WebServiceApi instance which will hold all HTTP requests
 *
 * @okHttpClient Factory for HTTP calls
 * @moshi Serialization Adapter
 */
private fun createWebServiceApi(okHttpClient: OkHttpClient, moshi: Moshi): WebServiceApi {
    val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.REST_SERVICE_BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    return retrofit.create(WebServiceApi::class.java)
}

/**
 * Create a OkHttpClient which is used to send HTTP requests and read their responses.
 *
 * @loggingInterceptor logging interceptor
 */
private fun createOkHttpClient(
    sslSocketFactory: SSLSocketFactory,
    trustAllCerts: Array<TrustManager>,
    loggingInterceptor: HttpLoggingInterceptor
): OkHttpClient {
    return OkHttpClient.Builder()
        .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
        .addInterceptor(loggingInterceptor)
        .readTimeout(defaultTimeout, TimeUnit.SECONDS)
        .connectTimeout(defaultTimeout, TimeUnit.SECONDS)
        .build()
}

/**
 * Create an OkHttp interceptor which logs HTTP request and response data.
 */
private fun createLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
}

private fun createTrustManagers(): Array<TrustManager> {
    // Create a trust manager that does not validate certificate chains
    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()

        @Throws(CertificateException::class)
        override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
        }

        @Throws(CertificateException::class)
        override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
        }
    })
    return trustAllCerts
}

private fun createSSLSocketFactory(trustAllCerts: Array<TrustManager>): SSLSocketFactory {
    // Install the all-trusting trust manager
    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, trustAllCerts, java.security.SecureRandom())

    // Create an ssl socket factory with our all-trusting manager
    return sslContext.socketFactory
}