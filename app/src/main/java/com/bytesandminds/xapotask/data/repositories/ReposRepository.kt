package com.bytesandminds.xapotask.data.repositories

import com.bytesandminds.xapotask.data.model.Repo
import com.bytesandminds.xapotask.data.model.RepoDetail
import com.bytesandminds.xapotask.data.network.Result
import com.bytesandminds.xapotask.data.network.WebServiceApi

class ReposRepository(private val webServiceApi: WebServiceApi) {

    suspend fun getAllRepos(): Result<List<Repo>> {
        val response = webServiceApi.getAllRepos()
        return if (response.isSuccessful) {
            Result.Success(response.body()!!)
        } else {
            Result.Error(response.message())
        }
    }

    suspend fun getRepoByName(owner: String, repo: String): Result<RepoDetail> {
        val response = webServiceApi.getRepoByName(owner, repo)
        return if (response.isSuccessful) {
            Result.Success(response.body()!!)
        } else {
            Result.Error(response.message())
        }
    }

}